import frame from "./frame";
import show from "./show";
import page from "./page";
import element from "./element";
import preview from "./preview";
import message from "./message";

const initialState = {
  show: true,
  page: {
    pages: [
      undefined
    ],
    pageIndex: 0
  },
  frame: {
    name: "element",
    data: {}
  },
  preview: {
    visible: false
  },
  element: undefined,
  messages: {
    text: "",
    wait: undefined
  }
};

/*
 * Forager reducer
 */
function reducer(state = initialState, action) {
  switch ( action.type ) {
  default:
    return {
      frame: frame(state.frame, action),
      show: show(state.show, action),
      page: page(state.page, action),
      element: element(state.element, action),
      preview: preview(state.preview, action),
      message: message(state.message, action)
    };
  }
}

export default reducer;

import React from "react";

/*
 * Message
 * -------
 *
 * A message is a simple text string that is displayed to the user. An optional
 * wait prop can also be passed, which indicates how long to wait before fading
 * out the message.
 */
export default React.createClass({
  getInitialState: function() {
    return {
      text: "",
      faded: true
    }
  },
  componentWillMount: function() {
    this.setState({
      text: this.props.text,
      faded: false
    });
  },
  componentWillReceiveProps: function(nextProps) {
    this.setState({
      text: nextProps.text,
      faded: false
    });
  },
  render: function() {
    return (
      <div className="message">
        {this.state.text}
      </div>
    );
  },
  componentDidMount: function() {
    this.fade();
  },
  componentDidUpdate: function() {
    this.fade();
  },
  fade: function() {
    clearTimeout(this.timeout);
    const { wait } = this.props;
    if ( wait !== undefined && this.state.faded === false ) {
      this.timeout = setTimeout(() => {
        this.setState({
          text: "",
          faded: true
        });
      }, wait);
    }
  },
  timeout: undefined
});

import React from "react";

import { PosButton, NegButton } from "../Buttons";
import NoSelectMixin from "../NoSelectMixin";

import { createElement } from "../../helpers/page";
import { allSelect, count, select } from "../../helpers/selection";
import { highlight, unhighlight } from "../../helpers/markup";

export default React.createClass({
  highlight: "query-check",
  getInitialState: function() {
    return {
      type: "single",
      value: 0,
      optional: false
    };
  },
  saveHandler: function(event) {
    event.preventDefault();
    const { type, value, optional } = this.state;
    const { css, parent } = this.props;
    // all value must be set
    if ( type === "all" && value === "" ) {
      this.props.message("Name for type \"all\" elements cannot be empty");
      return;
    }
    const ele = createElement(css, type, value, optional);
    // generate the list of elements for the new element
    ele.elements = select(parent.elements, ele.selector, ele.spec);
    ele.parent = parent;
    parent.children.push(ele);
    // if saving a selector that selects "select" elements, add a child selector
    // to match option elements
    if ( allSelect(ele.elements) ) {
      const optionsChild = createElement("option", "all", "option", false);
      optionsChild.elements = select(ele.elements, optionsChild.selector, optionsChild.spec);
      optionsChild.parent = ele;
      ele.children.push(optionsChild);
    }
    this.props.save(ele);
  },
  cancelHandler: function(event) {
    event.preventDefault();
    this.props.cancel();
  },
  setSpec: function(type, value) {
    this.setState({
      type: type,
      value: value
    });
  },
  toggleOptional: function(event) {
    this.setState({
      optional: event.target.checked
    });
  },
  render: function() {
    const { parent, css } = this.props;
    const elementCount = count(parent.elements, css);
    return (
      <div className="frame spec-form">
        <div className="info">
          <div className="line">
            CSS Selector: {css}
          </div>
          <SpecForm count={elementCount}
                    setSpec={this.setSpec}/>
          <div className="line">
            <label>
              Optional: <input type="checkbox"
                               checked={this.state.optional}
                               onChange={this.toggleOptional} />
            </label>
          </div>
        </div>
        <div className="buttons">
          <PosButton text="Save" click={this.saveHandler} />
          <NegButton text="Cancel" click={this.cancelHandler} />
        </div>
      </div>
    );
  },
  componentWillMount: function() {
    const elements = select(
      this.props.parent.elements,
      this.props.css, {
      type: this.state.type,
      value: this.state.value
    });
    highlight(elements, this.highlight);
  },
  componentWillUpdate: function(nextProps, nextState) {
    unhighlight(this.highlight);
    const elements = select(
      nextProps.parent.elements,
      nextProps.css, {
      type: nextState.type,
      value: nextState.value
    });
    highlight(elements, this.highlight);
  },
  componentWillUnmount: function() {
    unhighlight(this.highlight);
  }
});

const SpecForm = React.createClass({
  mixins: [NoSelectMixin],
  getInitialState: function() {
    return {
      type: "single",
      value: 0
    };
  },
  setType: function(event) {
    const type = event.target.value;
    let value;
    if ( type === "single" ) {
      value = 0;
    } else if ( type === "all" ) {
      value = "";
    }
    this.props.setSpec(type, value);
    this.setState({
      type: type,
      value: value
    });
  },
  setValue: function(event) {
    let value = event.target.value;
    if ( this.state.type === "single" ) {
      value = parseInt(value, 10);
    }
    this.setSpec(this.state.type, value);
    this.setState({
      value: value
    });
  },
  setSpec: function(type, value) {
    this.props.setSpec(type, value);
  },
  _singleValue: function() {
    const { value } = this.state;
    const options = Array.from(new Array(this.props.count)).map((u, i) => {
      return (
        <option key={i} value={i}>{i}</option>
      );
    });
    return (
      <select value={value}
              onChange={this.setValue} >
        {options}
      </select>
    );
  },
  _allValue: function() {
    return (
      <input type="text"
             value={this.state.value}
             onChange={this.setValue} />
    );
  },
  render: function() {
    const valueChooser = this.state.type === "single" ? this._singleValue() : this._allValue();
    return (
      <div ref="parent">
        <div className="line">
          Type:
          <label>single <input type="radio"
                               name="type"
                               value="single"
                               checked={this.state.type === "single"}
                               onChange={this.setType} />
          </label>
          <label>all <input type="radio"
                               name="type"
                               value="all"
                               checked={this.state.type === "all"}
                               onChange={this.setType} />
          </label>
        </div>
        <div className="line">
          Value: {valueChooser}
        </div>
      </div>
    );
  }
})
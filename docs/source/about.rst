About
=====

`Forager <https://github.com/pshrmn/forager>`_ is a Chrome extension that allows you to easily "hunt and gather" data. It provides the user with a way to easily create CSS selector based rules to select elements within a page.
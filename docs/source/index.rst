Forager Documentation
=========================

View project on `GitHub <https://github.com/pshrmn/forager>`_

Guide:
^^^^^^

.. toctree::
   :maxdepth: 2

   about
   installation
   tutorial
   license
